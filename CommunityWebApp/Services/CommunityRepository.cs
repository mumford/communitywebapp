﻿using CommunityWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommunityWebApp.Services
{
    public class CommunityRepository : IRepository<Community>
    {
        private DataContext _context;


        // List<Community> _communities;

        public CommunityRepository(DataContext context)
        {
            _context = context;
        }
        public bool Add(Community item)
        {
            try
            {

               
                _context.Add(item);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {

                //throw;
                return false;
            }
        }

        public bool Delete(Community item)
        {
            throw new NotImplementedException();
        }

        public bool Edit(Community item)
        {
            throw new NotImplementedException();
        }

        public Community Get(int id)
        {
            if(_context.Communities.Count(x => x.CommunityId == id)>0)
            {
                return _context.Communities.FirstOrDefault(x => x.CommunityId == id);
            }
            { return null; }
        }

        public IEnumerable<Community> GetAll()
        {
            return _context.Communities;
        }
    }
}
