﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace CommunityWebApp.Models
{
    [Table("communities")]
    public class Community
    {
        public Community()
        {
        }
      
        //public Community(string connectionString)
        //{
        //    this.ConnectionString = connectionString;
        //}
        // //public string ConnectionString { get; set; }
        //private MySqlConnection GetConnection()
        //{
        //    return new MySqlConnection(ConnectionString);
        //}
        [Key]
        public int CommunityId { get; set; }
        [Required(ErrorMessage ="Name is Required")]
        public string CommunityName { get; set; }
        public int CommunityType { get; set; }
        public string CommunityDescription { get; set; }
        public string CommunityCode { get; set; }
        public DateTime DateCreated { get; set; }
        //public byte CommunityImage { get; set; }


       
    }
}

