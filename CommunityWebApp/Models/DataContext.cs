﻿using CommunityWebApp.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CommunityWebApp.Models
{
    public class DataContext : DbContext
    {
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    var builder = new ConfigurationBuilder()
        //        .SetBasePath(Directory.GetCurrentDirectory())
        //        .AddJsonFile("appsettings.json");

        //    var configuration = builder.Build();
        //    optionsBuilder.UseMySql(configuration["ConnectionStrings:RemoteMySqlServer"]);

        //}

        public DataContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Community> Communities { get; set; }
    }
}
