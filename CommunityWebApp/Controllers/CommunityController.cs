﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommunityWebApp.Models;
using CommunityWebApp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CommunityWebApp.Controllers
{ 
    [Route("Community")]
    public class CommunityController : Controller
    {
       // private DataContext db = new DataContext();
        //List<Community> _community;
        IRepository<Community> _comrepo;
    
    public CommunityController(IRepository<Community> community) //DI
        {
            
            _comrepo = community;
        }

   // [Route("")]
    [Route("index")]
   // [Route("~/")]
    public IActionResult Index()
    {
            //ViewBag.communities = db.Communities.ToList();
            ViewBag.communities = _comrepo.GetAll();
            return View();
    }

    [HttpGet]
        public IActionResult AddCommunity()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddCommunity(Community community)
        {
            Community item = new Community()
            {
                CommunityName = community.CommunityName,
                CommunityCode =community.CommunityCode
            };

            _comrepo.Add(item);
            return View();
        }


        // GET: Community
       

        // GET: Community/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Community/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Community/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Community/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Community/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Community/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Community/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}